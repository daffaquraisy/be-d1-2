import { DataSource } from "typeorm";
import dotenv from 'dotenv'
// import { Player } from './enitities/Player';

dotenv.config()

export const dataSource = new DataSource({
  type: "postgres",
  url: process.env.DB,
  // entities: [Player],
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  synchronize: true,
})
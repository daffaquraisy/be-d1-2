import "reflect-metadata"
import express from 'express';
import { dataSource } from './typeorm.config';
import * as bodyParser from 'body-parser';
import "reflect-metadata"
import cors from 'cors'
import dotenv from 'dotenv'
import { Request, Response } from 'express';
import cacheMiddleware from "./utils/cache";

dotenv.config()

// const boot = async () => {
const app = express();

const corsOptions: {
  origin: string[];
} = {
  origin: [
    "http://localhost:8080",
    "http://localhost:3000",
    "http://localhost:7788",
    "http://localhost:5173",
    "http://localhost:5174",
    "http://10.50.1.35",
    "http://10.50.3.202",
    "http://10.50.3.205",
    "http://10.50.1.35:5173",
    "http://10.50.1.35:9000",
    "http://10.50.1.35:8080",
    "http://10.50.3.202:5173",
    "http://10.50.3.202:5174",
    "http://10.50.3.205:5173",
    "http://10.50.3.205:5174",
    "http://10.50.3.202:8000",
    "http://10.50.3.202:8000",
    "http://127.0.0.1:5173",
    "http://127.0.0.1:5174",
    "http://127.0.0.1:8000",
    "http://127.0.0.1:8000",
    "https://trade.ntxsolution.com",
    "https://risk.ntxsolution.com",
    "https://trade.ntxsolution.com/",
    "https://risk.ntxsolution.com/",
    "https://osint.ntxsolution.com/",
    "https://osint.ntxsolution.com"
  ],
};


app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cacheMiddleware(120));

dataSource.initialize()

app.get("/", (_req: Request, res: Response) => {
  res.json({ message: "Hello" });
});

require("./routes/d1.routes")(app);


app.listen(process.env.PORT, () => {
  console.log("server start at port http://localhost:" + process.env.PORT)
})
// }

// boot().catch(err => console.log(err))

module.exports = app;

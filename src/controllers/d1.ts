import { dataSource } from '../typeorm.config';
import { Request, Response } from 'express';

export const getTradeExIm = async (req: Request, res: Response) => {

  interface QueryParams {
    country_id?: string;
    year?: string;
    fromcountry?: string;
  }

  const queryParams: QueryParams = {
    country_id: req.query.country_id as string ?? "",
    year: req.query.year as string ?? "",
    fromcountry: req.query.fromcountry as string ?? "",
  };

  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("trade_exim", null)
      .where("country_id like :country_id", { country_id: `%${queryParams.country_id}%` })
      .andWhere("year like :year", { year: `%${queryParams.year}%` })
      .andWhere("fromcountry like :fromcountry", { fromcountry: `%${queryParams.fromcountry}%` })
      .orderBy("updated_at")
      .getRawMany();

    let arr: any = [];

    data.map((e) => {
      let nd = new Date(e.updated_at);

      let dateOne =
        nd.getFullYear() +
        "-" +
        ("0" + (nd.getMonth() + 1)).slice(-2) +
        "-" +
        ("0" + nd.getDate()).slice(-2);

      arr.push({
        id: e.id,
        year: e.year,
        fromcountry: e.fromcountry,
        country: e.country,
        country_id: e.country_id,
        iso3: e.iso3,
        trade_value_first_exporte_last: e.trade_value_first_exporte_last,
        trade_value_second_exporte_last: e.trade_value_second_exporte_last,
        trade_value_first_exporte_growth: e.trade_value_first_exporte_growth,
        trade_value_first_exporte_growth_value:
          e.trade_value_first_exporte_growth_value,
        trade_value_second_exporte_growth:
          e.trade_value_second_exporte_growth,
        trade_value_second_exporte_growth_value:
          e.trade_value_second_exporte_growth_value,
        trade_value_delta: e.trade_value_delta,
        trade_value_growth_delta: e.trade_value_growth_delta,
        updated_at: dateOne,
      });
    });

    res.status(200).send({
      statusCode: 200,
      success: true,
      data: arr,
    });
  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
};

export const word = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("word_shift", null)
      .getRawMany();

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const laporan = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("laporan_g", null)
      .getRawMany();

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const geopol = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("geopol", null)
      .getRawMany();

    let arr: any = []

    data.map((e) => {
      let geo = e.geometry;
      let coords = geo
        .match(/\((.*)\)/)[1]
        .split(",")
        .map((coord: any) => coord.trim().split(" ").map(parseFloat));

      let polygon = {
        type: "Feature",
        geometry: {
          type: "Polygon",
          coordinates: [coords],
        },
        properties: {},
      };

      arr.push({
        admin: e.admin,
        geo_i: e.geo_i,
        first_a: e.first_a,
        pand_i: e.pand_i,
        sov_a3: e.sov_a3,
        iso_a3: e.iso_a3,
        continent: e.continent,
        region_un: e.region_un,
        subregion: e.subregion,
        name_id: e.name_id,
        filename: e.filename,
        geometry: polygon,
        city: e.city,
        lat: e.lat,
        long: e.long,
        pupolation: e.pupolation,
        capital: e.capital,
        latlong_cap: e.latlong_cap,
      });
    });

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const articles = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from('articles', null)
      .getRawMany();

    let arr: any = []

    data.map((e) => {
      let a = e.images.replace("{", "");
      let b = a.replace("}", "");
      let i = b.replace(/["']/g, "");
      let images = [i];

      arr.push({
        id: e.id,
        title: e.title,
        description: e.description,
        published_date: e.published_date,
        url: e.url,
        publisher: e.publisher,
        full_article: e.full_article,
        images: images,
      });
    });

    res.status(200).send({
      statusCode: 200,
      success: true,
      data: arr,
    });

  } catch (error) {
    console.log(error);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const economic_risk = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("economic_risk", null)
      .getRawMany();

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const social_risk = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("social_risk", null)
      .getRawMany();

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const political_risk = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("political_risk", null)
      .getRawMany();

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const legal_risk = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("legal", null)
      .getRawMany();

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const kikj = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("kikj", null)
      .getRawMany();

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const mapKikj = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("map_kikj", null)
      .getRawMany();

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const pb = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("pb", null)
      .getRawMany();

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const pji = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("pji", null)
      .getRawMany();

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const pjkj = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("pjkj", null)
      .getRawMany();

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const ppi = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("ppi", null)
      .getRawMany();

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const pti = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("pti", null)
      .getRawMany();

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const ptinter = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("ptinter", null)
      .getRawMany();

    res.status(200).send({
      statusCode: 200,
      success: true,
      data
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const din = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("din", null)
      .getRawMany();

    let arr: any = [];

    data.map(e => {
      arr.push({
        domains: e.domains,
        judul_laporan: e.judul_laporan,
        isi_artikel: e.isi_artikel,
        tanggal: e.tanggal,
        images: e.images,
        iso_a3: e.iso_a3,
        name_id: e.name_id,
        gatra: e.gatra,
        radar_gatra: e.radar_gatra ? JSON.parse(e.radar_gatra.replace(/'/g, '"')) : null,
        radar_ocean: e.radar_ocean ? JSON.parse(e.radar_ocean.replace(/'/g, '"')) : null,
      })
    })

    res.status(200).send({
      statusCode: 200,
      success: true,
      data: arr
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}

export const dws = async (_req: Request, res: Response) => {
  try {

    const data = await dataSource
      .createQueryBuilder()
      .select()
      .from("dws", null)
      .getRawMany();

    let arr: any = [];

    data.map(e => {
      arr.push({
        tanggal: e.tanggal,
        gatra: e.gatra,
        max_count: e.max_count,
        word_shift: e.word_shift ? JSON.parse(e.word_shift.replace(/'/g, '"')) : null,
      })
    })

    res.status(200).send({
      statusCode: 200,
      success: true,
      data: arr
    })

  } catch (err) {
    console.log(err);
    res.status(500).send({
      statusCode: 500,
      success: false,
      message: "Cannot fetch data.",
    });
  }
}
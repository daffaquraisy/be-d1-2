import {
  getTradeExIm,
  word,
  articles,
  laporan,
  geopol,
  economic_risk,
  legal_risk,
  social_risk,
  political_risk,
  kikj,
  mapKikj,
  pb,
  pji,
  pjkj,
  pti,
  ptinter,
  ppi,
  din,
  dws
} from '../controllers/d1.test';
import { testGet } from '../utils/tdd';

let dataDws = [
  {
    tanggal: "16 Nov 2022",
    gatra: "Sosial Budaya",
    max_count: 1,
    word_shift: [
      {
        gatra: "Ekonomi",
        words: "employment",
        count: 121,
        sentiment: "positive"
      },
      {
        gatra: "Ekonomi",
        words: "business",
        count: 40,
        sentiment: "positive"
      },
    ]
  },
]
testGet('/api/v1/d1/dws', '200 - dws', 200, dataDws, dws)

let dataDin = [
  {
    domains: "DW (English)",
    judul_laporan: "Slovenia introduces marriage equality - DW (English)",
    isi_artikel: "",
    tanggal: "16 Nov 2022",
    images: "https://static.dw.com/image/63762364_6.jpg",
    iso_a3: "SVN",
    name_id: "Slovenia",
    gatra: "Sosial Budaya",
    radar_gatra: {
      Politik: 0.13,
      Ekonomi: 0.43,
      "Pertahanan dan Keamanan": 0,
      Ideologi: 0.02,
      "Sosial Budaya": 1
    },
    radar_ocean: {
      Openness: 0.88,
      Extraversion: 0.12,
      Agreeableness: 0.55,
      Neuroticism: 0.84,
      Conscientiousness: 2
    }
  },
];
testGet('/api/v1/d1/din', '200 - din', 200, dataDin, din)

let dataPtiIn = [
  {
    id: 1,
    country: "Korea Selatan",
    section_id: 1,
    section: "Animal Products",
    hs2_id: 101,
    hs2: "Live animals",
    hs4_id: 10106,
    hs4: "Other Animals",
    year: "2020",
    flow: "Exports",
    measure: "Trade Value",
    value: "198105.0"
  },
];
testGet('/api/v1/d1/ptinter', '200 - ptinter', 200, dataPtiIn, ptinter)

let dataPti = [
  {
    id: 1,
    section_id: 1,
    section: "Animal Products",
    hs2_id: 101,
    hs2: "Live animals",
    hs4_id: 10101,
    hs4: "Horses",
    year: "2020",
    flow: "Imports",
    measure: "Trade Value",
    value: "762883.0",
    country: "Indonesia"
  },
];
testGet('/api/v1/d1/pti', '200 - pti', 200, dataPti, pti)

let dataPjkj = [
  {
    id: 265,
    country: "Korea Selatan",
    year: "2020",
    service: "Transportation",
    service_value: "27712900000",
    flow: "Exports"
  },
  {
    id: 266,
    country: "Korea Selatan",
    year: "2020",
    service: "Personal, cultural, and recreational services",
    service_value: "1107600000",
    flow: "Exports"
  },
];
testGet('/api/v1/d1/pjkj', '200 - pjkj', 200, dataPjkj, pjkj)

let dataPji = [
  {
    id: 121,
    service: "Travel",
    service_id: "2.0",
    service_value: "16910744064",
    year: "2019",
    flow: "Exports"
  },
  {
    id: 122,
    service: "Other business services",
    service_id: "9.0",
    service_value: "6591619100",
    year: "2019",
    flow: "Exports"
  },
];
testGet('/api/v1/d1/pji', '200 - pji', 200, dataPji, pji)

let dataPb = [
  {
    id: 1,
    parent_service: "Royalties and license fees",
    parent_service_id: "8",
    parent_service_value: "73129968",
    service: "Royalties and license fees",
    service_id: "8.0",
    service_value: "73129968",
    year: "2020",
    bilateral: "Jerman - Indonesia"
  },
  {
    id: 2,
    parent_service: "Other business services",
    parent_service_id: "9",
    parent_service_value: "73129968",
    service: "Miscellaneous business, professional, and technical services",
    service_id: "9.3",
    service_value: "70844657",
    year: "2020",
    bilateral: "Jerman - Indonesia"
  },
];
testGet('/api/v1/d1/pb', '200 - pb', 200, dataPb, pb)

let dataMapKikj = [
  {
    id: 1,
    country: "Angola",
    iso_3: "ago",
    year: "2020",
    landscape: "Indonesia - Jerman",
    measure: "Trade Value Export",
    value: "-37652892.99999999"
  },
];
testGet('/api/v1/d1/mapKikj', '200 - mapKikj', 200, dataMapKikj, mapKikj)

let dataKikj = [
  {
    id: 301,
    section_id: 6,
    section: "Chemical Products",
    hs4: "Acyclic Hydrocarbons",
    hs4_id: 62901,
    rca_f: "0.7283159513493467",
    rca_s: "3.763020978334222",
    year: "2019",
    landscape: "Indonesia - Korea Selatan",
    advantage: "Produk Korea Selatan"
  },
];
testGet('/api/v1/d1/kikj', '200 - kikj', 200, dataKikj, kikj)

let dataPoliticalRisk = [
  {
    id: 163,
    admin: "Libya",
    political_r: "92.96",
    state_c: "88.89",
    political_leg: "88.89",
    policy_vol: "88.89",
    pol_vio: "100.0",
    geo_r: "100.0",
    sov_a3: "LBY",
    iso_a3: "LBY",
    continent: "Africa",
    region_un: "Africa",
    subregion: "Northern Africa",
    name_id: "Libya",
    filename: "LBY.geojson",
    geometry: "",
    city: "Tarabulus (Tripoli)",
    lat: "32.8752",
    long: "13.1875"
  },
];
testGet('/api/v1/d1/political_risk', '200 - political_risk', 200, dataPoliticalRisk, political_risk)

let dataSocialRisk = [
  {
    id: 321,
    admin: "Palestine",
    social_r: "100.0",
    inter_o: "77.78",
    civil_sr: "97.78",
    corruption: "91.11",
    lab_unrest: "87.78",
    facil_pv: "91.11",
    sov_a3: "IS1",
    iso_a3: "PSE",
    continent: "Asia",
    region_un: "Asia",
    subregion: "Western Asia",
    name_id: "Palestina",
    filename: "PSE.geojson",
    geometry: "",
    city: "Al-Quds[East Jerusalem]",
    lat: "31.7834",
    long: "35.2339",
    pupolation: null as any,
    capital: null as any,
    latlong_cap: null as any
  },
];
testGet('/api/v1/d1/social_risk', '200 - social_risk', 200, dataSocialRisk, social_risk)

let dataLegalRisk = [
  {
    id: 319,
    admin: "Iraq",
    legal_r: "100.0",
    gov_t: "100.0",
    eoc: "100.0",
    soc: "66.67",
    rb: "100.0",
    rol: "100.0",
    sov_a3: "IRQ",
    iso_a3: "IRQ",
    continent: "Asia",
    region_un: "Asia",
    subregion: "Western Asia",
    name_id: "Irak",
    filename: "IRQ.geojson",
    geometry: "",
    city: "Baghdad",
    lat: "33.3406",
    long: "44.4009",
    pupolation: null as any,
    capital: null as any,
    latlong_cap: null as any
  },
];
testGet('/api/v1/d1/legal_risk', '200 - legal_risk', 200, dataLegalRisk, legal_risk)

let dataEcoRisk = [
  {
    id: 160,
    admin: "Venezuela",
    economic_risk: "100.0",
    transfer_risk: "100.0",
    export_risk: "83.33",
    pfbalance: "100.0",
    r_gdp: "100.0",
    level: "77.78",
    sov_a3: "VEN",
    iso_a3: "VEN",
    continent: "South America",
    region_un: "Americas",
    subregion: "South America",
    name_id: "Venezuela",
    filename: "VEN.geojson",
    geometry: "",
    city: "Caracas",
    lat: "10.488",
    long: "-66.8792"
  },
];
testGet('/api/v1/d1/economic_risk', '200 - economic_risk', 200, dataEcoRisk, economic_risk)

let dataGeopol = [
  {
    id: 287,
    admin: "Japan",
    geo_i: "82.02",
    first_a: "Pro Ukraine ",
    pand_i: "73.91",
    sov_a3: "JPN",
    iso_a3: "JPN",
    continent: "Asia",
    region_un: "Asia",
    subregion: "Eastern Asia",
    name_id: "Jepang",
    filename: "JPN.geojson",
    geometry: "",
    city: "Tokyo",
    lat: "35.6895",
    long: "139.6917",
    pupolation: null as any,
    capital: null as any,
    latlong_cap: null as any
  },
];
testGet('/api/v1/d1/geopol', '200 - geopol', 200, dataGeopol, geopol)

let dataLaporan = [
  {
    id: 1,
    admin: "Russia",
    judul_laporan: "Ukraina Akui Tak Mampu Lawan Rusia Jika Menyerang Pakai Rudal Iran - detikNews",
    tanggal_laporan: "Tue, 31 Jan 2023 08:38:19 GMT",
    isi_laporan: "Angkatan Udara Ukraina menyatakan pasukannya tidak akan mampu melawan serangan-serangan Rusia yang menggunakan rudal balistik buatan Iran. Peringatan itu dilontarkan saat Moskow diperkirakan akan menerima pasokan rudal balistik buatan Teheran.'Rusia masih bersedia menerima UAV (kendaraan udara tanpa awak) dan rudal-rudal Fateh serta Zolfaghar dari Iran. Itu merupakan rudal-rudal balistik. Kami tidak memiliki sarana untuk mengalahkan mereka,' ucap juru bicara Komando Angkatan Udara Ukraina, Yurii Ihnat, seperti dilansir CNN, Selasa (31/1/2023).Pada November tahun lalu, Iran dilaporkan sedang bersiap mengirimkan sekitar 1.000 senjata lainnya ke Rusia untuk digunakan dalam perang di Ukraina. Pasokan senjata itu disebut akan mencakup rudal balistik jarak dekat jenis permukaan-ke-permukaan dan lebih banyak drone tempur.ADVERTISEMENT SCROLL TO RESUME CONTENTInformasi itu didasarkan pada informasi sejumlah pejabat dari salah satu negara Barat yang memantau secara saksama program persenjataan Iran.Laporan Reuters pada Oktober lalu, yang mengutip dua pejabat Iran dan dua diplomat Iran, menyebut Teheran berjanji memasok senjata-senjata itu ke Rusia.'Rusia telah meminta lebih banyak drone dan rudal balistik Iran dengan akurasi yang ditingkatkan, terutama kelompok rudal Fateh dan Zolfaghar,' sebut salah satu diplomat Iran kepada Reuters.Pemerintah Iran, pada November tahun lalu, mengakui telah mengirimkan drone dalam jumlah terbatas ke Rusia sekitar beberapa bulan sebelum invasi dilancarkan ke Ukraina. Teheran membantah telah memasok peralatan militer untuk digunakan Rusia dalam pertempuran di Ukraina.Simak berita selengkapnya di halaman selanjutnya.Saksikan juga 'Joe Biden Tak Akan Kirim Jet Tempur F-16 ke Ukraina':[Gambas:Video 20detik]",
    jenis_kode: "Lapin",
    jenis_laporan: "Laporan Informasi",
    tujuan: "Deputi-I",
    lat: "64.6863136",
    lon: "97.7453061",
    aspek: "Ideologi; Sosial Budaya",
    agent: "Kasuari 01",
    sov_a3: "RUS",
    iso_a3: "RUS",
    continent: "Europe",
    region_un: "Europe",
    subregion: "Eastern Europe",
    name_id: "Rusia",
    filename: "RUS.geojson",
    geometry: ""
  },
];
testGet('/api/v1/d1/laporan', '200 - laporan', 200, dataLaporan, laporan)

let dataArticles = [
  {
    id: 1,
    title: "Indonesia sends warship to monitor Chinese coast guard vessel - CNN",
    description: "Indonesia sends warship to monitor Chinese coast guard vessel  CNN",
    published_date: "2023-01-16T00:00:00.000Z",
    url: "https://www.cnn.com/2023/01/15/asia/indonesia-warship-china-natuna-sea-intl-hnk/index.html",
    publisher: "CNN",
    full_article: "",
    images: [
      ""
    ]
  },
]
testGet('/api/v1/d1/articles', '200 - articles', 200, dataArticles, articles)

const dataWord = [
  {
    id: 1,
    word: "Investment",
    sentiment: "Positive",
    count: 42,
    topics: "Economics",
    date: "2023-01-17T00:00:00.000Z"
  },
  {
    id: 2,
    word: "Debt",
    sentiment: "Negative",
    count: 27,
    topics: "Economics",
    date: "2023-01-17T00:00:00.000Z"
  },
]
testGet('/api/v1/d1/word', '200 - word', 200, dataWord, word)

let dataMapTrade = [
  {
    id: 1000,
    year: "2019",
    fromcountry: "South Korea",
    country: "Lithuania",
    country_id: "eultu",
    iso3: "ltu",
    trade_value_first_exporte_last: "25838198",
    trade_value_second_exporte_last: "343257558.0",
    trade_value_first_exporte_growth: "-0.146502184",
    trade_value_first_exporte_growth_value: "-4435105",
    trade_value_second_exporte_growth: "0.126533215",
    trade_value_second_exporte_growth_value: "38554995",
    trade_value_delta: "317419360.0",
    trade_value_growth_delta: "27.30353991",
    updated_at: "2023-02-09"
  },
  {
    id: 3,
    year: "2019",
    fromcountry: "South Korea",
    country: "Benin",
    country_id: "afben",
    iso3: "ben",
    trade_value_first_exporte_last: "144629702",
    trade_value_second_exporte_last: "38035929.0",
    trade_value_first_exporte_growth: "-0.375680755",
    trade_value_first_exporte_growth_value: "-87030147",
    trade_value_second_exporte_growth: "-0.034667153",
    trade_value_second_exporte_growth_value: "-1365951",
    trade_value_delta: "-106593773.0",
    trade_value_growth_delta: "34.10136022",
    updated_at: "2023-02-09"
  },
]
testGet('/api/v1/d1/map/trade_exim', '200 - map - trade_exim', 200, dataMapTrade, getTradeExIm)

let dataPpi = [
  {
    id: 5167,
    country: "Angola",
    iso_3: "AGO",
    year: "2017",
    measure: "Trade Value",
    value: "106594647.0",
    flow: "Exports"
  },
  {
    id: 5168,
    country: "Burundi",
    iso_3: "BDI",
    year: "2017",
    measure: "Trade Value",
    value: "1506418.0",
    flow: "Exports"
  },
]
testGet('/api/v1/d1/ppi', '200 - ppi', 200, dataPpi, ppi)

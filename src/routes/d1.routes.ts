import * as express from 'express';
import { Request, Response, Router, NextFunction } from 'express';
import {
  getTradeExIm,
  word,
  articles,
  laporan,
  geopol,
  economic_risk,
  legal_risk,
  social_risk,
  political_risk,
  kikj,
  mapKikj,
  pb,
  pji,
  pjkj,
  pti,
  ppi,
  ptinter,
  din,
  dws
} from '../controllers/d1';
// import * as passport from 'passport';

// import { authenticate } from "../middlewares/auth";

module.exports = (app: any) => {

  app.use((_req: Request, res: Response, next: NextFunction) => {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });


  const router: Router = express.Router();

  router.get('/map/trade_exim', getTradeExIm);

  router.get('/word', word);
  router.get('/articles', articles);
  router.get('/laporan', laporan);
  router.get('/geopol', geopol);

  router.get('/economic_risk', economic_risk);
  router.get('/legal_risk', legal_risk);
  router.get('/social_risk', social_risk);
  router.get('/political_risk', political_risk);

  router.get('/kikj', kikj);
  router.get('/mapKikj', mapKikj);

  router.get('/pb', pb);
  router.get('/pji', pji);
  router.get('/pjkj', pjkj);
  router.get('/pti', pti);
  router.get('/ptinter', ptinter);
  router.get('/ppi', ppi);

  router.get('/din', din);
  router.get('/dws', dws);

  app.use("/api/v1/d1", router);

};


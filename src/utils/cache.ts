import { Request, Response, NextFunction } from 'express';
import cache from 'memory-cache';

interface CustomResponse extends Response {
  sendResponse?: any;
}

const cacheMiddleware = (duration: number) => {
  return (req: Request, res: CustomResponse, next: NextFunction) => {
    const key = '__express__' + req.originalUrl || req.url;
    const cachedBody = cache.get(key);
    if (cachedBody) {
      res.send(cachedBody);
      return;
    } else {
      res.sendResponse = res.send;
      res.send = ((body: any) => {
        cache.put(key, body, duration * 1000);
        (res as CustomResponse).sendResponse(body);
        delete res.sendResponse;
      }) as CustomResponse['send'];
      next();
    }
  };
};

export default cacheMiddleware;

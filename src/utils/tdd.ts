import { Request, Response } from 'express';

export const testGet = (title: string, nameTest: string, statusCode: number, array: any, func: Function) => {
  describe('GET ' + title, () => {
    let mockRequest: Partial<Request>;
    let mockResponse: Partial<Response>;
    let responseObject = {};

    beforeEach(() => {
      mockRequest = {};
      mockResponse = {
        statusCode: 0,
        send: jest.fn().mockImplementation((result) => {
          responseObject = result;
        })
      };
    });

    test(nameTest, () => {
      const expectedStatusCode = statusCode;
      const expectedResponse = {
        data: array
      };

      func(mockRequest as Request, mockResponse as Response);

      expect(mockResponse.statusCode).toBe(expectedStatusCode)
      expect(responseObject).toMatchSnapshot(expectedResponse)
    });
  })
}